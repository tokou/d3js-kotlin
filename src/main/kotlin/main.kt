import d3.select
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.w3c.dom.Element
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLDivElement

var job = Job()

val days = listOf(
    "Day 19" to { GlobalScope.launch { ui.day19() } },
    "Day 18" to { GlobalScope.launch { ui.day18() } },
    "Day 17" to { GlobalScope.launch { ui.day17() } }
)

fun main(args: Array<String>) {
    days.forEach { addButton(it.first, it.second) }
    job = days.first().second()
}

private fun addButton(
    title: String,
    callback: () -> Job
) {
    select<HTMLDivElement, Any>("#menu")
        .append<HTMLButtonElement>("button")
        .text(title)
        .on("click", { _, _, _ ->
            job.cancel()
            clearContent()
            job = callback()
        }, false)
}

private fun clearContent() {
    select<HTMLDivElement, Any>("#content").selectAll<Element, Any>("*").remove()
}
