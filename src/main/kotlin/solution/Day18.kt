
enum class Land {
    OPEN, WOODEN, LUMBERYARD
}

fun String.parseForest(): Array<Array<Land>> = split("\n")
    .map { line -> line.map { c -> c.toLand() }.toTypedArray() }.toTypedArray()

fun Char.toLand(): Land = when (this) {
    '|' -> Land.WOODEN
    '#' -> Land.LUMBERYARD
    '.' -> Land.OPEN
    else -> throw IllegalArgumentException()
}

fun Land.toChar(): Char = when (this) {
    Land.WOODEN -> '|'
    Land.LUMBERYARD -> '#'
    Land.OPEN -> '.'
}

fun Array<Array<Land>>.formatted(): String {
    val sb = StringBuilder()
    forEach { l ->
        l.forEach { c -> sb.append(c.toChar()) }
        sb.appendln()
    }
    return sb.toString()
}


fun Pair<Int, Int>.adj(): Set<Pair<Int, Int>> = setOf(
    first - 1 to second,
    first + 1 to second,
    first to second - 1,
    first to second + 1,
    first - 1 to second - 1,
    first + 1 to second - 1,
    first - 1 to second + 1,
    first + 1 to second + 1
)

fun Array<Array<Land>>.tick(): Array<Array<Land>> = Array(size) { j -> Array(this[j].size) { i ->
    val neighbors = (i to j)
        .adj()
        .filter { it.first in 0 until this[j].size && it.second in 0 until size }
        .map { this[it.second][it.first] }
        .groupBy { it }
        .mapValues { it.value.size }
    when(val current = this[j][i]) {
        Land.OPEN ->  if ((neighbors[Land.WOODEN] ?: 0) >= 3) Land.WOODEN else current
        Land.WOODEN -> if ((neighbors[Land.LUMBERYARD] ?: 0) >= 3) Land.LUMBERYARD else current
        Land.LUMBERYARD ->
            if((neighbors[Land.LUMBERYARD] ?: 0) >= 1 && (neighbors[Land.WOODEN] ?: 0) >= 1) Land.LUMBERYARD
            else Land.OPEN
    }
} }

fun day18part1(input: Array<Array<Land>>): Int = input.steps(10).countScore()
fun day18part2(input: Array<Array<Land>>): Int = input.steps(1_000_000_000).countScore()

typealias Forest = Array<Array<Land>>

fun Forest.countScore(): Int =
    sumBy { l -> l.count { it == Land.WOODEN } } * sumBy { l -> l.count { it == Land.LUMBERYARD } }

private fun Forest.steps(time: Int, print: Boolean = false): Array<Array<Land>> {
    var forest = this
    if (print) println(forest.formatted())
    for (i in 1..time) {
        if (i % 1_000_000 == 0) println(i)
        forest = forest.tick()
        if (print) println(forest.formatted())
    }
    return forest
}

fun runForest(forest: Forest, time: Int): Sequence<Forest> = sequence {
    var state = forest
    yield(state)
    for (i in 1..time) {
        state = state.tick()
        yield(state)
    }
}