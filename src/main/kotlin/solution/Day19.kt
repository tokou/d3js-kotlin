package solution

import kotlin.math.sqrt

/**
 * strt | goto r3bg
 * next | r5 = 1
 * bbbb | r2 = 1
 * altr | r1 = r5 * r2
 * .... | if (r1 == r3) goto cccc else goto aftr
 * .... |
 * .... |
 * cccc | r0 += r5
 * aftr | r2 = r3
 * .... | r2 += 1
 * .... | if (r2 > r3) goto eeee else goto altr
 * .... |
 * eeee | r5 += 1
 * .... | if (r5 > r3) ? goto exit else goto bbbb
 * .... |
 * .... |
 * exit | goto endd
 * r3bg | r3 += 2
 * .... | r3 = r3 ^ 2
 * .... | r3 *= 19 * 11
 * .... | r1 = ((r1 + 5) * 22) + 2
 * .... | r3 += r1
 * .... |
 * .... |
 * .... |
 * .... | if (r0 == 0) goto next else goto r1bg
 * .... |
 * r1bg | r1 = ((27 * 28) + 29) * 30 * 14 * 32
 * .... | r3 += r1
 * .... |
 * .... |
 * .... |
 * .... |
 * .... |
 * .... | r0 = 0
 * .... | goto next
 * endd |
 *
 *
 * fun grow_r3() {
 *     r3 = if (r0 == 0) 948
 *     else 10551348
 * }
 *
 * fun main() {
 *     grow_r3()
 *     r5 = 1
 *     r2 = 1
 *     do {
 *         r1 = r5 * r2
 *         if (r1 == r3) { r0 += r5 }
 *         r2 += 1
 *         if (r2 > r3) {
 *             r5 += 1
 *             r2 = 1
 *         }
 *     } while (r5 <= r3 || r2 <= r3)
 * }
 *
 * p = 1
 * r = 0
 * a, b = 1
 * m = (p == 0) 948 ? 10551348
 * for (a in 1..r3+1) {
 *     for (b in 1..r3+1) {
 *         if (a * b == m) r += a
 *     }
 * }
 *
 * import kotlin.math.sqrt
 *
 * fun divSum(v: Int): Int {
 *     val sq = sqrt(v.toDouble()).toInt()
 *     var r = 0
 *     for (i in 2..sq) {
 *         if (v % i == 0) {
 *             if (i == v / i) r++
 *             else r += i + v / i
 *         }
 *     }
 *     return r + 1
 * }
 *
 * fun main() {
 *     println(divSum(948))
 *     println(divSum(10551348))
 * }
 *
 */


enum class Op(val op: IntArray.() -> Unit) {
    ADDR({ C().reg().value = A().reg().value + B().reg().value }),
    ADDI({ C().reg().value = A().reg().value + B() }),
    MULR({ C().reg().value = A().reg().value * B().reg().value }),
    MULI({ C().reg().value = A().reg().value * B() }),
    BANR({ C().reg().value = A().reg().value and B().reg().value }),
    BANI({ C().reg().value = A().reg().value and B().toLong() }),
    BORR({ C().reg().value = A().reg().value or B().reg().value }),
    BORI({ C().reg().value = A().reg().value or B().toLong() }),
    SETR({ C().reg().value = A().reg().value }),
    SETI({ C().reg().value = A().toLong() }),
    GTIR({ C().reg().value = if (A() > B().reg().value) 1 else 0 }),
    GTRI({ C().reg().value = if (A().reg().value > B()) 1 else 0 }),
    GTRR({ C().reg().value = if (A().reg().value > B().reg().value) 1 else 0 }),
    EQIR({ C().reg().value = if (A().toLong() == B().reg().value) 1 else 0 }),
    EQRI({ C().reg().value = if (A().reg().value == B().toLong()) 1 else 0 }),
    EQRR({ C().reg().value = if (A().reg().value == B().reg().value) 1 else 0 });

    fun translate(ops: IntArray): String = when (this) {
        ADDR -> opFormat(ops = ops, o = "+")
        ADDI -> opFormat(ops = ops, o = "+", i2 = true)
        MULR -> opFormat(ops = ops, o = "*")
        MULI -> opFormat(ops = ops, o = "*", i2 = true)
        BANR -> opFormat(ops = ops, o = "&")
        BANI -> opFormat(ops = ops, o = "&", i2 = true)
        BORR -> opFormat(ops = ops, o = "|")
        BORI -> opFormat(ops = ops, o = "|", i2 = true)
        SETR -> opFormat(ops = ops)
        SETI -> opFormat(ops = ops, i1 = true)
        GTIR -> opFormat(ops = ops, o = ">", comp = true, i1 = true)
        GTRI -> opFormat(ops = ops, o = ">", comp = true, i2 = true)
        GTRR -> opFormat(ops = ops, o = ">", comp = true)
        EQIR -> opFormat(ops = ops, o = "==", comp = true, i1 = true)
        EQRI -> opFormat(ops = ops, o = "==", comp = true, i2 = true)
        EQRR -> opFormat(ops = ops, o = "==", comp = true)
    }

    fun jump(ops: IntArray, ip: Int, i: Int): String = when (this) {
        ADDR -> opFormat(ip = ip, i = i, ops = ops, o = "+")
        ADDI -> opFormat(ip = ip, i = i, ops = ops, o = "+", i2 = true)
        MULR -> opFormat(ip = ip, i = i, ops = ops, o = "*")
        MULI -> opFormat(ip = ip, i = i, ops = ops, o = "*", i2 = true)
        BANR -> opFormat(ip = ip, i = i, ops = ops, o = "&")
        BANI -> opFormat(ip = ip, i = i, ops = ops, o = "&", i2 = true)
        BORR -> opFormat(ip = ip, i = i, ops = ops, o = "|")
        BORI -> opFormat(ip = ip, i = i, ops = ops, o = "|", i2 = true)
        SETR -> opFormat(ip = ip, i = i, ops = ops)
        SETI -> opFormat(ip = ip, i = i, ops = ops, i1 = true)
        GTIR -> opFormat(ip = ip, i = i, ops = ops, o = ">", comp = true, i1 = true)
        GTRI -> opFormat(ip = ip, i = i, ops = ops, o = ">", comp = true, i2 = true)
        GTRR -> opFormat(ip = ip, i = i, ops = ops, o = ">", comp = true)
        EQIR -> opFormat(ip = ip, i = i, ops = ops, o = "==", comp = true, i1 = true)
        EQRI -> opFormat(ip = ip, i = i, ops = ops, o = "==", comp = true, i2 = true)
        EQRR -> opFormat(ip = ip, i = i, ops = ops, o = "==", comp = true)
    }
}

fun opFormat(
    ops: IntArray, i1: Boolean = false, o: String? = null, i2: Boolean = false, comp: Boolean = false, ip: Int? = null, i: Int = 0
) = StringBuilder().apply {
    val isJmp = ip != null && ip == ops.C()
    if (isJmp) {
        append("JMP ")
    } else {
        append("R[")
        append(ops.C())
        append("] = ")
    }
    if (comp) append("(")
    if (!i1 && ip != null && ip == ops.A()) {
        append(i)
    } else {
        if (!i1) append("R[")
        append(ops.A())
        if (!i1) append("]")
    }
    if (o != null) {
        append(" $o ")
        if (!i2 && ip != null && ip == ops.B()) {
            append(i)
        } else {
            if (!i2) append("R[")
            append(ops.B())
            if (!i2) append("]")
        }
    }
    if (isJmp) append(" + 1")
    if (comp) append(") ? 1 : 0")
}.toString()

fun IntArray.A() = this[0]
fun IntArray.B() = this[1]
fun IntArray.C() = this[2]
fun Int.reg() = registers[this]
fun LongArray.toRegisters() = mapIndexed { i, v -> Register(i, v) }.toTypedArray()
fun Array<Register>.values() = LongArray(6) { this[it].value }.toList()

data class Register(val index: Int, var value: Long = 0L)

var registers = Array(6) { Register(it) }

fun Array<Register>.reset() = forEach { r -> r.value = 0 }

typealias Instruction = Pair<Op, IntArray>

fun reg(i: Int): Long = registers[i].value
fun inc(i: Int) { registers[i].value += 1 }

fun Instruction.run(): Unit = first.op(second)

fun day19part1(input: List<String>): Int {
    val ip = parseIp(input)
    val instructions = parseInstructions(input)
    val initial = listOf(0L, 0L, 0L, 0L, 0L, 0L).toLongArray().toRegisters()
    runProgram(initial, ip, instructions)
    return reg(0).toInt()
}

fun divSum(value: Int): Int {
    val root = sqrt(value.toDouble()).toInt()
    var result = 0
    for (i in 2..root) {
        if (value % i == 0) {
            if (i == value / i) result++
            else result += i + value / i
        }
    }
    return value + result + 1
}

fun day19part2(input: List<String>): Int {
    val ip = parseIp(input)
    val instructions = parseInstructions(input)
    val initial = listOf(10908048L, 0L, 0L, 0L, 0L, 0L).toLongArray().toRegisters()
    registers = initial
    var prevR3 = 0L
    while (reg(ip) in 0 until instructions.size && reg(3) != prevR3 && prevR3 == 0L) {
        prevR3 = reg(3)
        instructions[reg(ip).toInt()].run()
        inc(ip)
    }
    return divSum(reg(3).toInt())
}

private fun runProgram(
    initial: Array<Register>,
    ip: Int,
    instructions: List<Pair<Op, IntArray>>
) {
    registers = initial
    while (reg(ip) in 0 until instructions.size) {
        instructions[reg(ip).toInt()].run()
        inc(ip)
    }
}

fun parseIp(input: List<String>) = input.first().split(" ").last().toInt()

fun parseInstructions(input: List<String>): List<Pair<Op, IntArray>> = input.drop(1).map { l ->
    val instr = l.split(" ")
    val op = Op.valueOf(instr.first().toUpperCase())
    val operands = instr.drop(1).map { it.toInt() }.toIntArray()
    op to operands
}
