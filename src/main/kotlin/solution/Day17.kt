import Ground.*

typealias Soil = Array<Array<Ground>>

fun day17part1(vertical: List<Vertical>, horizontal: List<Horizontal>): Soil {
    val bounds = findBounds(vertical, horizontal)
    val size = findSize(bounds)
    val origin = findOrigin(bounds)
    val ground = initGround(size, origin, vertical, horizontal)
    ground.gravity(500 - origin.first to 0)
    return ground
}

enum class Ground {
    SAND, SPRING, WATER, CLAY, FLOWING
}

typealias Bounds = Pair<Pair<Int, Int>, Pair<Int, Int>>
typealias Horizontal = Pair<IntRange, Int>
typealias Vertical = Pair<Int, IntRange>

fun String.parseWalls(): Pair<List<Vertical>, List<Horizontal>> {
    val lines = split("\n")

    val vertical = lines
        .map { "x=(\\d+), y=(\\d+)\\.\\.(\\d+)".toRegex().matchEntire(it)?.groupValues }
        .filter { it != null }
        .map { it!![1].toInt() to it[2].toInt()..it[3].toInt() }
    val horizontal = lines
        .map { "y=(\\d+), x=(\\d+)\\.\\.(\\d+)".toRegex().matchEntire(it)?.groupValues }
        .filter { it != null }
        .map { it!![2].toInt()..it[3].toInt() to it[1].toInt() }
    return vertical to horizontal
}

fun Soil.waterCount(): Int = sumBy { l -> l.sumBy { if (it == WATER) 1 else 0 } }

private fun Soil.overflow(source: Pair<Int, Int>): Boolean {
    var lX = source.first
    var rX = source.first
    var spilled = false
    while (this[lX to source.second] != CLAY) {
        val below = this[lX to source.second + 1]
        if (below == SAND) {
            spilled = gravity(lX to source.second) || spilled
            break
        } else if (below == FLOWING) {
            spilled = true
            break
        }
        this[lX to source.second] = WATER
        lX -= 1
    }
    while (this[rX to source.second] != CLAY) {
        val below = this[rX to source.second + 1]
        if (below == SAND) {
            spilled = gravity(rX to source.second) || spilled
            break
        }
        else if (below == FLOWING) {
            spilled = true
            break
        }
        this[rX to source.second] = WATER
        rX += 1
    }
    if (spilled) {
        for (x in lX..rX) {
            if (this[x to source.second] == WATER) {
                this[x to source.second] = FLOWING
            }
        }
    }
    return spilled
}

private fun Soil.gravity(source: Pair<Int, Int>): Boolean {
    var newY = source.second
    while (this[source.first to newY] != CLAY) {
        this[source.first to newY] = FLOWING
        newY += 1
        if (newY >= size || newY < 0) {
            return true
        }
    }
    do {
        newY -= 1
    } while (!overflow(source.first to newY) && newY > source.second)
    return newY != source.second
}

private operator fun Soil.get(position: Pair<Int, Int>): Ground =
    this[position.second][position.first]
private operator fun Soil.set(position: Pair<Int, Int>, value: Ground) {
    this[position.second][position.first] = value
}

fun initGround(
    size: Pair<Int, Int>,
    origin: Pair<Int, Int>,
    vertical: List<Vertical>,
    horizontal: List<Horizontal>
): Soil {
    val g = Array(size.second) { Array(size.first) { SAND } }
    val xMin = origin.first
    val yMin = origin.second
    g[0][500 - xMin] = SPRING
    for ((x, ys) in vertical) {
        for (y in ys) {
            g[y - yMin][x - xMin] = CLAY
        }
    }
    for ((xs, y) in horizontal) {
        for (x in xs) {
            g[y - yMin][x - xMin] = CLAY
        }
    }
    return g
}

fun findOrigin(bounds: Bounds): Pair<Int, Int> = bounds.first.first to bounds.second.first

fun findSize(bounds: Bounds): Pair<Int, Int> {
    val (xBounds, yBounds) = bounds
    val (xMin, xMax) = xBounds
    val (yMin, yMax) = yBounds
    return (xMax - xMin + 1) to (yMax - yMin + 1)
}

fun findBounds(vertical: List<Pair<Int, IntRange>>, horizontal: List<Pair<IntRange, Int>>): Bounds {
    val yMin = 0
    val yMax = minOf(vertical.maxBy { it.second.last }!!.second.last, horizontal.maxBy { it.second }!!.second)
    val xMin = maxOf(vertical.minBy { it.first }!!.first, horizontal.minBy { it.first.first }!!.first.first)
    val xMax = maxOf(vertical.maxBy { it.first }!!.first, horizontal.maxBy { it.first.last }!!.first.last)
    return (xMin - 1 to xMax + 1) to (yMin - 1 to yMax)
}

fun Soil.formatted(): String {
    val sb = StringBuilder()
    val size = this[0].size
    if (size > 10) {
        sb.appendln()
        sb.append("   ")
        for (i in 0..9) sb.append(" ")
        for (j in 1..size/10) for (i in 0..9) if (j * 10 + i < size) sb.append(j)
    }
    sb.appendln()
    sb.append("  ")
    if (size > 10) sb.append(" ")
    for (i in 0 until size) sb.append(i % 10)
    sb.appendln()
    for (j in 0 until this.size) {
        if (size > 10 && j < 10) sb.append(" $j") else sb.append(j)
        sb.append(" ")
        for (i in 0 until this[0].size) {
            val c = when (this[j][i]) {
                WATER -> '~'
                CLAY -> '#'
                SPRING -> '+'
                SAND -> '.'
                FLOWING -> '|'
            }
            sb.append(c)
        }
        sb.appendln()
    }
    return sb.toString()
}
