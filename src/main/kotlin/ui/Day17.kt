package ui

import Soil
import d3.select
import day17part1
import findBounds
import findSize
import Ground
import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.CanvasRenderingContext2DSettings
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.get
import parseWalls
import kotlin.browser.document

fun day17() {
    val raw = document["day17"] as String
    val input = raw.parseWalls()
    val bounds = findBounds(input.first, input.second)
    val size = findSize(bounds)

    var unit = 40
    while (size.first * unit > 1200 && unit > 1) unit--

    val width = unit * size.first
    val height = unit * size.second

    val canvas = select<HTMLCanvasElement, Soil>("#content")
        .append<HTMLCanvasElement>("canvas")
        .attr("width", width)
        .attr("height", height)

    val options = CanvasRenderingContext2DSettings(false)
    val context = canvas.node()!!.getContext("2d", options) as CanvasRenderingContext2D

    initCanvas(context, width to height)
    val soil = day17part1(input.first, input.second)
    displayData(context, soil, unit.toDouble())
}


private fun displayData(context: CanvasRenderingContext2D, soil: Soil, unit: Double) {
    context.beginPath()
    context.fillStyle = Ground.CLAY.toColor()
    soil.forEachIndexed { j, l -> l.forEachIndexed { i, ground ->
        if (ground == Ground.CLAY) context.fillRect(i * unit, j * unit, unit, unit)
    } }
    context.fill()
    context.closePath()
    context.beginPath()
    context.fillStyle = Ground.WATER.toColor()
    soil.forEachIndexed { j, l -> l.forEachIndexed { i, ground ->
        if (ground == Ground.WATER) context.fillRect(i * unit, j * unit, unit, unit)
    } }
    context.fill()
    context.closePath()
    context.beginPath()
    context.fillStyle = Ground.FLOWING.toColor()
    soil.forEachIndexed { j, l -> l.forEachIndexed { i, ground ->
        if (ground == Ground.FLOWING) context.fillRect(i * unit, j * unit, unit, unit)
    } }
    context.fill()
    context.closePath()
}

private fun initCanvas(context: CanvasRenderingContext2D, size: Pair<Int, Int>) {
    context.beginPath()
    context.fillStyle = Ground.SAND.toColor()
    context.rect(0.0, 0.0, size.first.toDouble(), size.second.toDouble())
    context.fill()
    context.closePath()
}

private fun Ground.toColor(): String = when (this) {
    Ground.SAND -> "gold"
    Ground.FLOWING -> "dodgerblue"
    Ground.WATER -> "blue"
    Ground.CLAY -> "black"
    Ground.SPRING -> "green"
}

