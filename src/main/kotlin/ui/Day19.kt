package ui

import d3.Selection
import d3.addClasses
import d3.select
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.w3c.dom.*
import org.w3c.dom.events.KeyboardEvent
import solution.*
import kotlin.browser.document


var paused = true
var step = false
var speed = 4

suspend fun day19() = coroutineScope {
    val input = (document["day21"] as String).split("\n")
    val pCode = (document["day21pseudo"] as String).split("\n").toTypedArray()

    val ip = parseIp(input)
    val instructions = parseInstructions(input)
    registers = listOf(0L, 0L, 0L, 0L, 0L, 0L).toLongArray().toRegisters()

    val current = select<HTMLDivElement, Pair<Int, Instruction>>("#content")
        .append<HTMLParagraphElement>("p")
        .addClasses("current")
        .style("font-family", "monospace")

    val state = select<HTMLDivElement, Pair<Int, Instruction>>("#content")
        .append<HTMLParagraphElement>("p")
        .addClasses("state")
        .style("font-family", "monospace")

    val rows = select<HTMLDivElement, Pair<Int, Instruction>>("#content")
        .append<HTMLDivElement>("div")
        .style("float", "left")
        .style("width", "240px")
        .addClasses("rows")
        .selectAll<HTMLSpanElement, Instruction>("div")
        .data(instructions.toTypedArray())

    val translated = select<HTMLDivElement, Pair<Int, Instruction>>("#content")
        .append<HTMLDivElement>("div")
        .style("float", "left")
        .style("width", "320px")
        .addClasses("translated")
        .selectAll<HTMLSpanElement, Instruction>("div")
        .data(instructions.toTypedArray())

    val jumps = select<HTMLDivElement, Pair<Int, Instruction>>("#content")
        .append<HTMLDivElement>("div")
        .style("float", "left")
        .style("width", "300px")
        .addClasses("translated")
        .selectAll<HTMLSpanElement, Instruction>("div")
        .data(instructions.toTypedArray())

    val pseudo = select<HTMLDivElement, String>("#content")
        .append<HTMLDivElement>("div")
        .addClasses("fff")
        .style("float", "left")
        .style("width", "500px")
        .selectAll<HTMLSpanElement, Instruction>("div")
        .data(instructions.toTypedArray())

    val newPseudo = pseudo
        .data(pCode)
        .enter()
        .append<HTMLSpanElement>("span")
        .style("font-family", "monospace")
        .style("display", "block")
        .text { d, i, _ -> "${i.toString().padStart(2, '0')}  |  $d" }

    val newRows = rows
        .enter()
        .append<HTMLSpanElement>("span")
        .style("font-family", "monospace")
        .style("display", "block")
        .text { d, i, _ -> "${i.toString().padStart(2, '0')}  |  ${d.first.toString().toLowerCase()} ${d.second.joinToString(" ")}" }

    val newTranslated = translated
        .enter()
        .append<HTMLSpanElement>("span")
        .style("font-family", "monospace")
        .style("display", "block")
        .text { d, i, _ -> "${i.toString().padStart(2, '0')}  |  ${d.first.translate(d.second)}" }

    val newJumps = jumps
        .enter()
        .append<HTMLSpanElement>("span")
        .style("font-family", "monospace")
        .style("display", "block")
        .text { d, i, _ -> "${i.toString().padStart(2, '0')}  |  ${d.first.jump(d.second, 2, i.toInt())}" }

    document.body?.onkeyup = {
        if (it is KeyboardEvent) when (it.key) {
            " " -> paused = !paused
            "s" -> step = true
            "+" -> {
                speed++
                console.log(speed)
            }
            "-" -> {
                speed = maxOf(1, speed - 1)
                console.log(speed)
            }
        }
    }

    launch {
        while (reg(ip) in 0 until instructions.size) {
            val doRun = !paused || step
            step = false
            val i = reg(ip).toInt()
            if (doRun) instructions[i].run()
            updateCurrent(current, i)
            updateState(state)
            updateInstruction(newRows, i)
            updateInstruction(newTranslated, i)
            updateInstruction(newJumps, i)
            updateInstruction(newPseudo, i)
            delay((8 * speed).toLong())
            if (doRun) inc(ip)
        }
    }
}

fun updateCurrent(
    container: Selection<HTMLParagraphElement, Pair<Int, Instruction>, HTMLElement, Any>,
    ip: Int
) {
    container.transition().text("IP: $ip").duration(20)
}

fun updateState(
    container: Selection<HTMLParagraphElement, Pair<Int, Instruction>, HTMLElement, Any>
) {
    val registerValues = registers.values().joinToString("\t") { it.toString().padStart(20, '0') }
    container
        .transition()
        .text("regs: $registerValues").duration(10)
}

private fun updateInstruction(
    rows: Selection<HTMLSpanElement, *, *, *>,
    index: Int
) {
    val color = if (paused) "red" else "white"
    rows
        .style("background-color", "white")
    rows
        .filter<HTMLSpanElement> { _, i, _ -> index == i }
        .transition()
        .on("start") { _, _, _ ->
            rows.filter<HTMLSpanElement> { _, i, _ -> index == i }.style("background-color", "red")
        }
        .style("background-color", color)
        .duration(if (paused) 0 else 80 * speed)
}