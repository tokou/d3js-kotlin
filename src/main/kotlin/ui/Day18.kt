package ui

import Forest
import Land
import d3.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLSpanElement
import org.w3c.dom.get
import org.w3c.dom.svg.SVGGElement
import org.w3c.dom.svg.SVGRectElement
import org.w3c.dom.svg.SVGSVGElement
import parseForest
import runForest
import kotlin.browser.document

suspend fun day18() = coroutineScope {
    val raw = document["day18"] as String
    val input = raw.parseForest()
    val size = input[0].size to input.size

    val unit = 10
    val time = 1_000_000_000

    val grid = select<HTMLDivElement, Forest>("#content")
        .svg(size.first * unit, size.second * unit)

    val count = select<HTMLDivElement, Int>("#content")
        .append<HTMLSpanElement>("span")

    launch {
        var i = 0
        val steps = runForest(input, time).iterator()
        while (isActive && steps.hasNext()) {
            displayData(steps.next(), unit, grid)
            delay(16)
            i++
            count.node()?.innerText = i.toString()
        }
    }
}

private fun displayData(state: Forest, unit: Int, grid: Selection<SVGSVGElement, Forest, HTMLElement, Any>) {

    val rows = grid
        .selectAll<SVGGElement, Forest>(".row")
        .data(state)

    val newRows = rows
        .enter()
        .g()
        .addClasses("row")
        .attr("data-index") { _, i, _ -> i }

    val squares = rows
        .selectAll<SVGRectElement, Array<Land>>(".square")
        .data({ d, _, _ -> d})

    newRows
        .selectAll<SVGRectElement, Array<Land>>(".square")
        .data({ d, _, _ -> d})
        .enter()
        .append<SVGRectElement>("rect")
        .addClasses("square")
        .attr("width", unit)
        .attr("height", unit)
        .attr("x") { _, i, _ ->  i.toInt() * unit }
        .attr("y") { _, i, g -> g[i.toInt()].parentNode.getAttribute("data-index") * unit }
        .style("fill",  { d, _, _ -> d.toColor() })

    squares
        .transition()
        .duration(12)
        .style("fill",  { d, _, _ -> d.toColor() })
}

private fun Land.toColor(): String = when (this) {
    Land.WOODEN -> "green"
    Land.LUMBERYARD -> "olive"
    Land.OPEN -> "lightgrey"
}
