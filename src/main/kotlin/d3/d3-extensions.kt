@file:Suppress("unused")
package d3


fun <G : BaseType, D, P : BaseType, F> Selection<G, D, P, F>.addClasses(names: String): Selection<G, D, P, F> =
    classed(names, true)

fun <G : BaseType, D, P : BaseType, F> Selection<G, D, P, F>.removeClasses(names: String): Selection<G, D, P, F> =
    classed(names, false)

fun <G : BaseType, D, P : BaseType, F> Selection<G, D, P, F>.hasClasses(names: String): Boolean = classed(names)
