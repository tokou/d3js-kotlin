@file:Suppress("unused")
package d3

import org.w3c.dom.svg.*

// SVG

fun <G : BaseType, D, P : BaseType, F> Selection<G, D, P, F>.svg() = append<SVGSVGElement>("svg")
fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.line() = append<SVGLineElement>("line")
fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.rectangle() = append<SVGRectElement>("rect")
fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.circle() = append<SVGCircleElement>("circle")
fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.ellipse() = append<SVGEllipseElement>("ellipse")
fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.g() = append<SVGGElement>("g")
fun <S : EnterElement, D, P : BaseType, F> Selection<S, D, P, F>.g() = append<SVGGElement>("g")
fun <S : EnterElement, D, P : SVGGraphicsElement, F> Selection<S, D, P, F>.rectangle() = append<SVGRectElement>("rect")

fun <G : BaseType, D, P : BaseType, F> Selection<G, D, P, F>.svg(width: Number, height: Number) =
    svg().width(width).height(height)

fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.line(
    topLeft: Pair<Number, Number>, bottomRight: Pair<Number, Number>
) = line().topLeft(topLeft.first, topLeft.second).bottomRight(bottomRight.first, bottomRight.second)

fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.rectangle(
    topLeft: Pair<Number, Number>, width: Number, height: Number
) = rectangle().topLeft(topLeft.first, topLeft.second).width(width).height(height)

fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.circle(
    center: Pair<Number, Number>, radius: Number
) = circle().center(center.first, center.second).radius(radius)

fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.ellipse(
    center: Pair<Number, Number>, radius: Pair<Number, Number>
) = ellipse().center(center.first, center.second).radius(radius.first, radius.second)

// SVG

fun <D, P : BaseType, F> Selection<SVGSVGElement, D, P, F>.width(width: Number) = attr("width", width)
fun <D, P : BaseType, F> Selection<SVGSVGElement, D, P, F>.height(height: Number) = attr("height", height)

// Line

fun <D, P : BaseType, F> Selection<SVGLineElement, D, P, F>.topLeft(top: Number, left: Number) =
    attr("x1", top).attr("y1", left)
fun <D, P : BaseType, F> Selection<SVGLineElement, D, P, F>.bottomRight(bottom: Number, right: Number) =
    attr("x2", bottom).attr("y2", right)
fun <D, P : BaseType, F> Selection<SVGLineElement, D, P, F>.strokeWidth(width: Number) =
    attr("stroke-width", width)

// Rectangle

fun <D, P : BaseType, F> Selection<SVGRectElement, D, P, F>.width(width: Number) = attr("width", width)
fun <D, P : BaseType, F> Selection<SVGRectElement, D, P, F>.height(height: Number) = attr("height", height)

fun <D, P : BaseType, F> Selection<SVGRectElement, D, P, F>.width(fn: ValueFn<SVGRectElement, D, Number>) =
    attr("width", fn)
fun <D, P : BaseType, F> Selection<SVGRectElement, D, P, F>.height(fn: ValueFn<SVGRectElement, D, Number>) =
    attr("height", fn)

fun <D, P : BaseType, F> Selection<SVGRectElement, D, P, F>.topLeft(top: Number, left: Number) =
    attr("x", top).attr("y", left)

// Circle

fun <D, P : BaseType, F> Selection<SVGCircleElement, D, P, F>.center(x: Number, y: Number) =
    attr("cx", x).attr("cy", y)

fun <D, P : BaseType, F> Selection<SVGCircleElement, D, P, F>.centerX(cx: Number) = attr("cx", cx)
fun <D, P : BaseType, F> Selection<SVGCircleElement, D, P, F>.centerY(cy: Number) = attr("cy", cy)

fun <D, P : BaseType, F> Selection<SVGCircleElement, D, P, F>.centerX(fn: ValueFn<SVGCircleElement, D, Number>) =
    attr("cx", fn)
fun <D, P : BaseType, F> Selection<SVGCircleElement, D, P, F>.centerY(fn: ValueFn<SVGCircleElement, D, Number>) =
    attr("cy", fn)

fun <D, P : BaseType, F> Selection<SVGCircleElement, D, P, F>.radius(radius: Number) = attr("r", radius)

fun <D, P : BaseType, F> Selection<SVGCircleElement, D, P, F>.radius(fn: ValueFn<SVGCircleElement, D, Number>) =
    attr("r", fn)

// Ellipse

fun <D, P : BaseType, F> Selection<SVGEllipseElement, D, P, F>.center(x: Number, y: Number) =
    attr("cx", x).attr("cy", y)
fun <D, P : BaseType, F> Selection<SVGEllipseElement, D, P, F>.radius(x: Number, y: Number) =
    attr("rx", x).attr("ry", y)

// Text

fun <D, P : BaseType, F> Selection<SVGTextElement, D, P, F>.x(x: Number) = attr("x", x)
fun <D, P : BaseType, F> Selection<SVGTextElement, D, P, F>.y(y: Number) = attr("y", y)

fun <D, P : BaseType, F> Selection<SVGTextElement, D, P, F>.x(fn: ValueFn<SVGTextElement, D, Number>) =
    attr("x", fn)
fun <D, P : BaseType, F> Selection<SVGTextElement, D, P, F>.y(fn: ValueFn<SVGTextElement, D, Number>) =
    attr("y", fn)

fun <D, P : BaseType, F> Selection<SVGTextElement, D, P, F>.dx(dx: Number) = attr("dx", dx)
fun <D, P : BaseType, F> Selection<SVGTextElement, D, P, F>.dy(dy: Number) = attr("dy", dy)

fun <D, P : BaseType, F> Selection<SVGTextElement, D, P, F>.dx(dx: String) = attr("dx", dx)
fun <D, P : BaseType, F> Selection<SVGTextElement, D, P, F>.dy(dy: String) = attr("dy", dy)

fun <D, P : BaseType, F> Selection<SVGTextElement, D, P, F>.dx(fn: ValueFn<SVGTextElement, D, Number>) =
    attr("dx", fn)
fun <D, P : BaseType, F> Selection<SVGTextElement, D, P, F>.dy(fn: ValueFn<SVGTextElement, D, Number>) =
    attr("dy", fn)

// Graphics

class Transform {
    private val sb = StringBuilder()
    fun skewX(skew: Number) { sb.append("skewX($skew) ") }
    fun skewY(skew: Number) { sb.append("skewY($skew) ") }
    fun scale(x: Number) { sb.append("scale($x) ") }
    fun scale(x: Number, y: Number) { sb.append("scale($x, $y) ") }
    fun translate(x: Number) { sb.append("translate($x) ") }
    fun translate(x: Number, y: Number) { sb.append("translate($x, $y) ") }
    fun rotate(angle: Number) { sb.append("rotate($angle) ") }
    fun matrix(xCoef: Pair<Number, Number>, yCoef: Pair<Number, Number>, constCoef: Pair<Number, Number>) {
        sb.append("matrix(${xCoef.first}, ${xCoef.second}, " +
                "${yCoef.first}, ${yCoef.second}, " +
                "${constCoef.first}, ${constCoef.second}) ")
    }
    override fun toString(): String = sb.toString()
}

fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.transform(t: Transform.() -> Unit) =
    attr("transform", Transform().apply(t).toString().trim())

fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.transform(
    fn: Transform.(datum: D, index: Number, groups: PseudoArray<S>) -> Unit
) = attr("transform") { datum, index, groups -> Transform().apply { fn(datum, index, groups) }.toString().trim() }

fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.fill(fill: String) = attr("fill", fill)

fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.fill(fn: ValueFn<S, D, String>) =
    attr("fill", fn)

fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.stroke(stroke: String) =
    attr("stroke", stroke)

fun <S : SVGGraphicsElement, D, P : BaseType, F> Selection<S, D, P, F>.stroke(fn: ValueFn<S, D, String>) =
    attr("stroke", fn)

fun <S : SVGGraphicsElement, D, P : BaseType, F> Transition<S, D, P, F>.fill(fn: ValueFn<S, D, String>) =
    attr("fill", fn)
