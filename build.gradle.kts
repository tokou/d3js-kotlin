import org.jetbrains.kotlin.gradle.dsl.KotlinJsOptions
import org.jetbrains.kotlin.gradle.frontend.KotlinFrontendExtension
import org.jetbrains.kotlin.gradle.frontend.npm.NpmExtension
import org.jetbrains.kotlin.gradle.frontend.webpack.WebPackExtension
import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

buildscript {

    repositories {
        mavenCentral()
        jcenter()
        maven("https://dl.bintray.com/kotlin/kotlin-eap/")
        maven("https://kotlin.bintray.com/kotlin-js-wrappers/")
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.11")
        classpath("org.jetbrains.kotlin:kotlin-frontend-plugin:0.0.44")
    }
}

plugins {
    id("kotlin2js") version "1.3.11"
}

apply {
    plugin("kotlin-dce-js")
    plugin("org.jetbrains.kotlin.frontend")
}

version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    compile(kotlin("stdlib-js"))
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:1.0.1")
}

configure<KotlinFrontendExtension> {
    downloadNodeJsVersion = "11.4.0"
    sourceMaps = true
    configure<NpmExtension> {
        dependency("kotlin", "1.3.11")
        dependency("d3", "5.7.0")
        dependency("kotlinx-coroutines-core", "1.0.1")
    }
    define("PRODUCTION", false)
    bundle<WebPackExtension>("webpack") {
        if (this is WebPackExtension) {
            bundleName = "main"
            contentPath = file("src/main/resources/web")
            proxyUrl = "http://localhost:8000"
            mode = if (defined["PRODUCTION"] as Boolean) "production" else "development"
        }
    }
}

tasks {
    val baseOptions: KotlinJsOptions.() -> Unit = {
        metaInfo = true
        sourceMap = true
        sourceMapEmbedSources = "always"
        moduleKind = "commonjs"
    }
    named<Kotlin2JsCompile>("compileKotlin2Js") {
        kotlinOptions(baseOptions)
        kotlinOptions {
            outputFile = "${project.buildDir.path}/js/${project.name}.js"
            main = "call"
        }
    }
    named<Kotlin2JsCompile>("compileTestKotlin2Js") {
        kotlinOptions(baseOptions)
        kotlinOptions {
            kotlinOptions.outputFile = "${project.buildDir.path}/js-tests/${project.name}-tests.js"
            main = "noCall"
        }
    }
}
